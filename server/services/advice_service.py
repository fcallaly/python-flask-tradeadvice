import datetime
import logging
import os

import pandas_datareader as pdr


LOG = logging.getLogger(__name__)


def get_advice(ticker):
    LOG.debug('get: ' + str(ticker))

    start_date = datetime.datetime.now() - datetime.timedelta(30)
    start_date = str(start_date)

    try:
        df = pdr.get_data_yahoo(ticker, start_date)[['Close']]
    except Exception as ex:
        LOG.error('Error getting data: ' + str(ex))
        return

    LOG.debug('Received: ' + str(df.shape) + ' from yahoo')

    df['30d mavg'] = df['Close'].rolling(window=21).mean()
    df['30d std'] = df['Close'].rolling(window=21).std()

    df['Upper Band'] = df['30d mavg'] + (df['30d std'] * 2)
    df['Lower Band'] = df['30d mavg'] - (df['30d std'] * 2)

    response = 'Hold'

    if df['Close'].iloc[-1] > df['Upper Band'].iloc[-1]:
        response = 'Sell'

    elif df['Close'].iloc[-1] < df['Lower Band'].iloc[-1]:
        response = 'Buy'

    LOG.debug(df.tail())
    return {'lastClose': df['Close'].iloc[-1],
            'upperBand': df['Upper Band'].iloc[-1],
            'lowerBand': df['Lower Band'].iloc[-1],
            'advice': response}
