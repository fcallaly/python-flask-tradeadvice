import json
import logging

from flask import jsonify, request, Response
from flask_restx import abort, Namespace, Resource
from server import app, api

from server.services import advice_service

LOG = logging.getLogger(__name__)

advice_namespace = Namespace('advice',
                             description='Interface for Advice Resource')

api.add_namespace(advice_namespace)


@advice_namespace.route("")
class Advice(Resource):

    @api.param('ticker', description='stock ticker', type='string')
    def get(self):
        if not request.args.get("ticker"):
            abort(400, message='Ticker is required')
        else:
            response = advice_service.get_advice(request.args.get("ticker"))

        if response is None:
            abort(404, message='Stock not found for ticker: ' +
                               str(request.args.get("ticker")))
        return Response(json.dumps(response, default=str),
                        mimetype="application/json")
